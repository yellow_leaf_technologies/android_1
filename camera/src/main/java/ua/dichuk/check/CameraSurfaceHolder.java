package ua.dichuk.check;


import android.hardware.Camera;
import android.view.SurfaceHolder;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.List;

import ua.dichuk.check.parameter.HRVParameter;
import ua.dichuk.check.parameter.HRVParameterEnum;
import ua.dichuk.check.utils.ImageProcessing;
import ua.dichuk.check.utils.TimeUnit;
import ua.dichuk.check.utils.Utils;

public class CameraSurfaceHolder implements SurfaceHolder.Callback {
    private CameraBaseManager manager;
    private Camera camera;

    private SurfaceHolder previewHolder;

    private int averageIndex = 0;
    private final int averageArraySize = 4;
    private int[] averageArray = new int[averageArraySize];
    private int beatsIndex = 0;
    private final int beatsArraySize = 3;
    private int[] beatsArray = new int[beatsArraySize];
    private double beats = 0;
    private long startTime = 0;

    private List<Double> intervalTimeList;

    private CameraSurfaceHolder() {

    }

    public CameraSurfaceHolder(CameraBaseManager manager, SurfaceHolder previewHolder) {
        this.manager = manager;
        this.previewHolder = previewHolder;
        camera = manager.getCamera();
        intervalTimeList = new ArrayList<>();
    }

    @Override
    public void surfaceCreated(android.view.SurfaceHolder holder) {
        try {
            camera.setPreviewDisplay(previewHolder);
            camera.setPreviewCallback(previewCallback);
        } catch (Throwable t) {

        }
    }

    @Override
    public void surfaceChanged(android.view.SurfaceHolder holder, int format, int width, int height) {
        Camera.Parameters parameters = camera.getParameters();
        parameters.setFlashMode(Camera.Parameters.FLASH_MODE_TORCH);
        Camera.Size size = Utils.getSmallestPreviewSize(width, height, parameters);
        if (size != null) {
            parameters.setPreviewSize(size.width, size.height);
        }
        camera.setParameters(parameters);
        camera.startPreview();
    }

    @Override
    public void surfaceDestroyed(android.view.SurfaceHolder holder) {

    }

    private Camera.PreviewCallback previewCallback = new Camera.PreviewCallback() {
        /**
         * {@inheritDoc}
         */
        @Override
        public void onPreviewFrame(byte[] data, Camera cam) {
            if (data == null) throw new NullPointerException();
            Camera.Size size = cam.getParameters().getPreviewSize();
            if (size == null) throw new NullPointerException();

            int width = size.width;
            int height = size.height;


            int imgAvg = ImageProcessing.decodeYUV420SPtoRedAvg(data.clone(), height, width);
            if (imgAvg == 0 || imgAvg == 255) {
                return;
            }
            int averageArrayAvg = 0;
            int averageArrayCnt = 0;
            for (int value : averageArray) {
                if (value > 0) {
                    averageArrayAvg += value;
                    averageArrayCnt++;
                }
            }

            int rollingAverage = (averageArrayCnt > 0) ? (averageArrayAvg / averageArrayCnt) : 0;
            Type newType = manager.getCurrentType();
            if (imgAvg < rollingAverage) {
                newType = Type.RED;
                if (newType != manager.getCurrentType()) {
                    beats++;
                }
            } else if (imgAvg > rollingAverage) {
                newType = Type.GREEN;
            }

            if (averageIndex == averageArraySize) averageIndex = 0;
            averageArray[averageIndex] = imgAvg;
            averageIndex++;

            // Transitioned from one state to another to the same
            if (newType != manager.getCurrentType()) {
                manager.setType(newType);
            }

            long endTime = System.currentTimeMillis();
            double totalTimeInSecs = (endTime - startTime) / 1000d;

            if (totalTimeInSecs >= 1) {
                double bps = (beats / totalTimeInSecs);
                int dpm = (int) (bps * 60d);
                if (dpm < 30 || dpm > 180) {
                    startTime = System.currentTimeMillis();
                    beats = 0;
                    return;
                }
                if (beatsIndex == beatsArraySize) beatsIndex = 0;
                beatsArray[beatsIndex] = dpm;
                beatsIndex++;

                int beatsArrayAvg = 0;
                int beatsArrayCnt = 0;
                for (int i = 0; i < beatsArray.length; i++) {
                    if (beatsArray[i] > 0) {
                        beatsArrayAvg += beatsArray[i];
                        beatsArrayCnt++;
                    }
                }
                int beatsAvg = (beatsArrayAvg / beatsArrayCnt);
                totalTimeInSecs = (totalTimeInSecs * 1000);
                intervalTimeList.add(totalTimeInSecs);
                manager.getObserver().onUpdateHeartRate(beatsAvg);

                if (intervalTimeList.size() > 4) {
                    double[] resData = new double[intervalTimeList.size()];
                    for (int i = 0; i < intervalTimeList.size(); i++) {
                        resData[i] = intervalTimeList.get(i);
                    }
                    RRData dataRR = RRData.createFromRRInterval(resData, TimeUnit.MILLISECOND);

                    HRVLibFacade facade = new HRVLibFacade(dataRR);

                    facade.setParameters(EnumSet.of(HRVParameterEnum.RMSSD));
                    List<HRVParameter> rmssdResult = facade.calculateParameters();

                    facade.setParameters(EnumSet.of(HRVParameterEnum.SDNN));
                    List<HRVParameter> sdnnResult = facade.calculateParameters();

                    facade.setParameters(EnumSet.of(HRVParameterEnum.HF, HRVParameterEnum.LF, HRVParameterEnum.VLF, HRVParameterEnum.LFHF));
                    List<HRVParameter> vlfResult = facade.calculateParameters();

                    manager.getObserver().onUpdateData(sdnnResult, rmssdResult, vlfResult);
                }
                startTime = System.currentTimeMillis();
                beats = 0;
            }
        }
    };

    void resetData() {
        averageIndex = 0;
        averageArray = new int[averageArraySize];
        beatsIndex = 0;
        beatsArray = new int[beatsArraySize];
        beats = 0;
        startTime = 0;
        intervalTimeList = new ArrayList<>();
    }
}
