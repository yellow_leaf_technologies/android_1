package ua.dichuk.check.utils;


import android.hardware.Camera;

import java.util.ArrayList;
import java.util.List;

import ua.dichuk.check.calc.PowerSpectrum;
import ua.dichuk.check.parameter.VLFCalculator;

public class Utils {

    public static Camera.Size getSmallestPreviewSize(int width, int height, Camera.Parameters parameters) {
        Camera.Size result = null;
        for (Camera.Size size : parameters.getSupportedPreviewSizes()) {
            if (size.width <= width && size.height <= height) {
                if (result == null) {
                    result = size;
                } else {
                    int resultArea = result.width * result.height;
                    int newArea = size.width * size.height;

                    if (newArea < resultArea) result = size;
                }
            }
        }
        return result;
    }

    /**
     * @return
     */
    public static double resultSDNN(List<Double> intervalTimeList) {
        int length = intervalTimeList.size();
        int allInterval = 0;
        for (Double interval : intervalTimeList) {
            allInterval += interval;
        }
        int average = allInterval / length;
        double res = 0;
        for (Double interval : intervalTimeList) {
            res += Math.pow((interval - average), 2);
        }
        double result = res / length;
        return Math.sqrt(result);
    }


    /**
     * @return
     */
    public static double resultrMSSD(List<Double> intervalTimeList) {
        double result = 0.0;
        int length = intervalTimeList.size();
        List<Double> sumList = new ArrayList<>();
        for (int i = 0; i < intervalTimeList.size() - 1; i++) {
            double first = intervalTimeList.get(i);
            double last = intervalTimeList.get(i + 1);
            double sum = Math.pow((first - last), 2);
            sumList.add(sum);
        }
        double sum = 0;
        if (sumList.size() > 0) {
            for (Double res : sumList) {
                sum += res;
            }
            result = sum / length;
            return Math.sqrt(result);
        }
        return result;
    }

    public static double resultVLF(List<Double> intervalTimeList) {
        int length = intervalTimeList.size();
        int allInterval = 0;
        for (Double interval : intervalTimeList) {
            allInterval += interval;
        }
        int average = allInterval / length;
        double res = 0;
        for (Double interval : intervalTimeList) {
            res += Math.pow((interval - average), 2);
        }
        double result = res / length;


        return result;
    }
}
