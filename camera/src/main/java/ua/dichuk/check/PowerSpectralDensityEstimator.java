package ua.dichuk.check;

import ua.dichuk.check.calc.PowerSpectrum;

public interface PowerSpectralDensityEstimator {

    PowerSpectrum calculateEstimate(RRData rr);
}

