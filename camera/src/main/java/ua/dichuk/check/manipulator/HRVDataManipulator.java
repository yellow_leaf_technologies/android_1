package ua.dichuk.check.manipulator;

import ua.dichuk.check.RRData;

public interface HRVDataManipulator {

    /**
     * Manipulates the given data and returns the manipulated data. The given data must at least contain three data-points.
     *
     * @param data data to manipulate.
     * @return Manipulated data.
     */
    RRData manipulate(RRData data);
}
