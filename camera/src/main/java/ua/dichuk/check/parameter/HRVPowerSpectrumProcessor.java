package ua.dichuk.check.parameter;

import ua.dichuk.check.calc.PowerSpectrum;

public interface HRVPowerSpectrumProcessor {

    HRVParameter process(PowerSpectrum ps);
}
