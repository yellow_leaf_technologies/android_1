package ua.dichuk.check;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.List;
import java.util.Set;

import ua.dichuk.check.calc.PowerSpectrum;
import ua.dichuk.check.manipulator.HRVCutToPowerTwoDataManipulator;
import ua.dichuk.check.manipulator.HRVMultiDataManipulator;
import ua.dichuk.check.manipulator.HRVSplineInterpolator;
import ua.dichuk.check.manipulator.HRVSubstractMeanManipulator;
import ua.dichuk.check.parameter.AmplitudeModeCalculator;
import ua.dichuk.check.parameter.BaevskyCalculator;
import ua.dichuk.check.parameter.HFCalculator;
import ua.dichuk.check.parameter.HRVDataProcessor;
import ua.dichuk.check.parameter.HRVParameter;
import ua.dichuk.check.parameter.HRVParameterEnum;
import ua.dichuk.check.parameter.LFCalculator;
import ua.dichuk.check.parameter.MeanCaclulator;
import ua.dichuk.check.parameter.ModeCalculator;
import ua.dichuk.check.parameter.MxDMnCalculator;
import ua.dichuk.check.parameter.NN50Calculator;
import ua.dichuk.check.parameter.PNN50Calculator;
import ua.dichuk.check.parameter.RMSSDCalculator;
import ua.dichuk.check.parameter.SD1Calculator;
import ua.dichuk.check.parameter.SD1SD2Calculator;
import ua.dichuk.check.parameter.SD2Calculator;
import ua.dichuk.check.parameter.SDNNCalculator;
import ua.dichuk.check.parameter.SDSDCalculator;
import ua.dichuk.check.parameter.VLFCalculator;

public class HRVLibFacade {

    private EnumSet<HRVParameterEnum> frequencyParams = EnumSet.of(HRVParameterEnum.LFHF, HRVParameterEnum.HF, HRVParameterEnum.LF, HRVParameterEnum.VLF);

    private Set<HRVParameterEnum> parameters = EnumSet.of(HRVParameterEnum.BAEVSKY, HRVParameterEnum.HF,
            HRVParameterEnum.LF, HRVParameterEnum.NN50, HRVParameterEnum.PNN50, HRVParameterEnum.RMSSD,
            HRVParameterEnum.SD1, HRVParameterEnum.SD2, HRVParameterEnum.SD1SD2, HRVParameterEnum.SDNN,
            HRVParameterEnum.SDSD, HRVParameterEnum.VLF);

    private HRVMultiDataManipulator frequencyDataManipulator = new HRVMultiDataManipulator();
    private HRVMultiDataManipulator filters = new HRVMultiDataManipulator();
    private RRData data;


    public HRVLibFacade(RRData data) {
        this.data = data;

        frequencyDataManipulator.addManipulator(new HRVSplineInterpolator(4));
        frequencyDataManipulator.addManipulator(new HRVCutToPowerTwoDataManipulator());
        frequencyDataManipulator.addManipulator(new HRVSubstractMeanManipulator());
    }

    /**
     * Calculates the HRV-Parameters specified in {@code parameters} for the
     * given RR-Data.
     *
     * @return List of HRV-Parameters
     */
    public List<HRVParameter> calculateParameters() {

        RRData filteredData = filters.manipulate(data);

        List<HRVParameter> allParams = new ArrayList<>();
        List<HRVDataProcessor> allCalculators = getAllHRVDataProcessors();

        for (HRVDataProcessor p : allCalculators) {
            allParams.add(p.process(filteredData));
        }

        if (containsOne(frequencyParams, parameters)) {
            allParams.addAll(calculateFrequencyParams(filteredData));
        }

        return allParams;
    }

    public void setParameters(Set<HRVParameterEnum> parameters) {
        this.parameters = parameters;
    }

    private List<HRVDataProcessor> getAllHRVDataProcessors() {
        List<HRVDataProcessor> processors = new ArrayList<>();

        for (HRVParameterEnum param : parameters) {
            HRVDataProcessor processor = getHRVDataProcessor(param);
            if (processor != null) {
                processors.add(processor);
            }
        }

        return processors;
    }

    private static <T extends Enum<T>> boolean containsOne(Set<T> setToTest, Set<T> set) {
        for (Object e : set) {
            if (setToTest.contains(e)) {
                return true;
            }
        }
        return false;
    }

    private List<HRVParameter> calculateFrequencyParams(RRData data) {
        List<HRVParameter> allParameters = new ArrayList<>();
        HRVParameter vlf;
        HRVParameter lf = null;
        HRVParameter hf = null;
        PowerSpectrum ps = getPowerSpectrum(data);
        if (parameters.contains(HRVParameterEnum.LF) || parameters.contains(HRVParameterEnum.LFHF)) {
            LFCalculator calcLF = new LFCalculator();
            lf = calcLF.process(ps);
            allParameters.add(lf);
        }

        if (parameters.contains(HRVParameterEnum.HF) || parameters.contains(HRVParameterEnum.LFHF)) {
            HFCalculator calcHF = new HFCalculator();
            hf = calcHF.process(ps);
            allParameters.add(hf);
        }
        if (parameters.contains(HRVParameterEnum.VLF)) {
            VLFCalculator calcVLF = new VLFCalculator();
            vlf = calcVLF.process(ps);
            allParameters.add(vlf);
        }
        if (parameters.contains(HRVParameterEnum.LFHF) && lf != null && hf != null) {
            allParameters.add(new HRVParameter(HRVParameterEnum.LFHF, lf.getValue() / hf.getValue(), ""));
        }
        return allParameters;

    }

    /**
     * Calculates the power spectrum of the given data.
     *
     * @param data Data to calculate the power spectrum from
     * @return power spectrum
     */
    public PowerSpectrum getPowerSpectrum(RRData data) {
        RRData manipulatedData = frequencyDataManipulator.manipulate(data);
        StandardPowerSpectralDensityEstimator estimator = new StandardPowerSpectralDensityEstimator();
        return estimator.calculateEstimate(manipulatedData);
    }

    private HRVDataProcessor getHRVDataProcessor(HRVParameterEnum e) {
        switch (e) {
            case AMPLITUDEMODE:
                return new AmplitudeModeCalculator();
            case BAEVSKY:
                return new BaevskyCalculator();
            case MEAN:
                return new MeanCaclulator();
            case MODE:
                return new ModeCalculator();
            case MXDMN:
                return new MxDMnCalculator();
            case NN50:
                return new NN50Calculator();
            case PNN50:
                return new PNN50Calculator();
            case RMSSD:
                return new RMSSDCalculator();
            case SDNN:
                return new SDNNCalculator();
            case SD1:
                return new SD1Calculator();
            case SD2:
                return new SD2Calculator();
            case SDSD:
                return new SDSDCalculator();
            case SD1SD2:
                return new SD1SD2Calculator();
            default:
                return null;
        }
    }
}
