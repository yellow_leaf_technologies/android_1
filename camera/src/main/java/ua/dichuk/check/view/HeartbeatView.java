package ua.dichuk.check.view;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.view.View;

import ua.dichuk.check.R;

public class HeartbeatView extends View {

    private static final Matrix matrix = new Matrix();
    private static final Paint paint = new Paint(Paint.ANTI_ALIAS_FLAG);

    private static Bitmap greenBitmap;
    private static Bitmap redBitmap;

    private static int parentWidth;
    private static int parentHeight;

    public HeartbeatView(Context context, AttributeSet attr) {
        super(context, attr);
        init();
    }

    public HeartbeatView(Context context) {
        super(context);
        init();
    }

    private void init() {
        greenBitmap = BitmapFactory.decodeResource(getResources(), R.drawable.ic_green);
        redBitmap = BitmapFactory.decodeResource(getResources(), R.drawable.ic_red);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);

        parentWidth = MeasureSpec.getSize(widthMeasureSpec);
        parentHeight = MeasureSpec.getSize(heightMeasureSpec);
        setMeasuredDimension(parentWidth, parentHeight);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void onDraw(Canvas canvas) {
        if (canvas == null) throw new NullPointerException();
        Bitmap bitmap;
//        if (CameraBaseManager.getInstance((AppCompatActivity) getContext()).getCurrentType() == Type.GREEN)
//            bitmap = greenBitmap;
//        else
        bitmap = redBitmap;
        if (bitmap == null)
            return;
        int bitmapX = bitmap.getWidth() / 2;
        int bitmapY = bitmap.getHeight() / 2;

        int parentX = parentWidth / 2;
        int parentY = parentHeight / 2;

        int centerX = parentX - bitmapX;
        int centerY = parentY - bitmapY;

        matrix.reset();
        matrix.postTranslate(centerX, centerY);
        canvas.drawBitmap(bitmap, matrix, paint);
    }

}
