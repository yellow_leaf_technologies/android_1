package ua.dichuk.check;

import android.content.Context;
import android.hardware.Camera;
import android.os.PowerManager;
import android.view.SurfaceHolder;

import androidx.appcompat.app.AppCompatActivity;

public class CameraBaseManager extends CheckPermission {
    private static CameraBaseManager manager;

    private Type type = Type.RED;

    private Camera camera;
    private AppCompatActivity activity;
    private PowerManager.WakeLock wakeLock;
    private CameraObserver observer;
    private CameraSurfaceHolder cameraSurfaceHolder;

    private CameraBaseManager(AppCompatActivity activity, CameraObserver observer, SurfaceHolder previewHolder) {
        this.activity = activity;
        this.observer = observer;
        init(previewHolder);
    }

    public CameraObserver getObserver() {
        return observer;
    }

    private void init(SurfaceHolder previewHolder) {
        initWakeLock();
        cameraSurfaceHolder = new CameraSurfaceHolder(this, previewHolder);
        previewHolder.addCallback(cameraSurfaceHolder);
        previewHolder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);
    }

    private void initWakeLock() {
        PowerManager pm = (PowerManager) activity.getSystemService(Context.POWER_SERVICE);
        wakeLock = pm.newWakeLock(PowerManager.FULL_WAKE_LOCK, "DoNotDimScreen");
        sendCamera();
    }

    public static CameraBaseManager getInstance(AppCompatActivity activity, CameraObserver observer, SurfaceHolder previewHolder) {
        if (manager == null) {
            manager = new CameraBaseManager(activity, observer, previewHolder);
        }
        return manager;
    }

    public Camera getCamera() {
        return camera;
    }

    public Type getCurrentType() {
        return type;
    }

    public void setType(Type type) {
        this.type = type;
    }


    public void sendCamera() {
        if (isPermissionGranted(activity)) {
            onRequestWriteReadPermission();
        } else {
            requestPermissions(activity);
        }
    }

    private void openCamera() {
        wakeLock.acquire(/*10 minutes*/);
        camera = Camera.open();
    }

    public void resetData() {
        cameraSurfaceHolder.resetData();
        observer.onResetData();
    }

    public void destroyCamera() {
        camera.setPreviewCallback(null);
        camera.stopPreview();
        camera.release();
        camera = null;
    }

    @Override
    protected void onRequestWriteReadPermission() {
        openCamera();
    }
}
