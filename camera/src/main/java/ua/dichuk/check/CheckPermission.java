package ua.dichuk.check;

import android.Manifest;
import android.content.pm.PackageManager;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

public abstract class CheckPermission {

    boolean isPermissionGranted(AppCompatActivity activity) {
        return ContextCompat.checkSelfPermission(activity, Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED;
    }

    void requestPermissions(AppCompatActivity activity) {
        ActivityCompat.requestPermissions(activity, new String[]{Manifest.permission.CAMERA}, 1);
        onRequestWriteReadPermission();
    }

    protected abstract void onRequestWriteReadPermission();

}
