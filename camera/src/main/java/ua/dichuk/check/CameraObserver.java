package ua.dichuk.check;

import java.util.List;

import ua.dichuk.check.parameter.HRVParameter;

public interface CameraObserver {

    void onError();

    void onResetData();

    void onUpdateData(List<HRVParameter> rmssdResult, List<HRVParameter> sdnnResult, List<HRVParameter> vlfResult);

    void onUpdateHeartRate(double heartRate);
}
