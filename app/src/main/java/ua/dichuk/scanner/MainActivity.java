package ua.dichuk.scanner;

import android.os.Bundle;
import android.view.SurfaceView;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import java.util.List;

import ua.dichuk.check.CameraBaseManager;
import ua.dichuk.check.CameraObserver;
import ua.dichuk.check.parameter.HRVParameter;

public class MainActivity extends AppCompatActivity implements CameraObserver {
    private SurfaceView preview;
    private TextView tvHeartrate, tvrMSSD, tvSDNN, tvVeryLowFrequency, tvLowFrequency, tvHighFrequency, tvAverageFrequency;
    private CameraBaseManager cameraManager;
    private Button btnReset;


    /**
     * {@inheritDoc}
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        init();
        cameraManager = CameraBaseManager.getInstance(this, this, preview.getHolder());
        updateDataUI(0, 0, 0, 0, 0, 0, 0);
    }

    private void init() {
        initUI();
        setListener();
    }

    private void initUI() {
        preview = findViewById(R.id.preview);

        tvHeartrate = findViewById(R.id.tv_heartrate);
        tvrMSSD = findViewById(R.id.tv_rmssd);
        tvSDNN = findViewById(R.id.tv_sdnn);
        tvVeryLowFrequency = findViewById(R.id.tv_very_low_frequency);
        tvLowFrequency = findViewById(R.id.tv_low_frequency);
        tvHighFrequency = findViewById(R.id.tv_high_frequency);
        tvAverageFrequency = findViewById(R.id.tv_average_frequency);
        btnReset = findViewById(R.id.btn_reset);
    }


    private void updateDataUI(int heartrateResult, double sddnResult, double rmssdResult, double veryLowFrequency, double lowFrequency, double highFrequency, double averageFrequency) {
        tvHeartrate.setText(getResources().getString(R.string.heartrate, String.valueOf(heartrateResult)));
        tvSDNN.setText(getResources().getString(R.string.sdnn, String.valueOf(sddnResult)));
        tvrMSSD.setText(getResources().getString(R.string.rMSSD, String.valueOf(rmssdResult)));
        tvVeryLowFrequency.setText(getResources().getString(R.string.very_low_frequency, String.valueOf(veryLowFrequency)));
        tvLowFrequency.setText(getResources().getString(R.string.low_frequency, String.valueOf(lowFrequency)));
        tvHighFrequency.setText(getResources().getString(R.string.high_frequency, String.valueOf(highFrequency)));
        tvAverageFrequency.setText(getResources().getString(R.string.average_frequency, String.valueOf(averageFrequency)));
    }

    private void setListener() {
        btnReset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cameraManager.resetData();
            }
        });
    }

    @Override
    public void onError() {
        Toast.makeText(getApplicationContext(), "Error!!!", Toast.LENGTH_LONG).show();
    }

    @Override
    public void onResetData() {
        updateDataUI(0, 0, 0, 0, 0, 0, 0);
    }

    @Override
    public void onUpdateData(List<HRVParameter> rmssdResult, List<HRVParameter> sdnnResult, List<HRVParameter> vlfResult) {
        tvSDNN.setText(getResources().getString(R.string.sdnn, String.valueOf(sdnnResult.get(0).getValue())));
        tvrMSSD.setText(getResources().getString(R.string.rMSSD, String.valueOf(rmssdResult.get(0).getValue())));
        tvHighFrequency.setText(getResources().getString(R.string.high_frequency, String.valueOf(vlfResult.get(0).getValue())));
        if (vlfResult.size() > 1)
            tvLowFrequency.setText(getResources().getString(R.string.low_frequency, String.valueOf(vlfResult.get(1).getValue())));
        if (vlfResult.size() > 2)
            tvVeryLowFrequency.setText(getResources().getString(R.string.very_low_frequency, String.valueOf(vlfResult.get(2).getValue())));
        if (vlfResult.size() > 3)
            tvAverageFrequency.setText(getResources().getString(R.string.average_frequency, String.valueOf(vlfResult.get(3).getValue())));
    }

    @Override
    public void onUpdateHeartRate(double heartRate) {
        tvHeartrate.setText(getResources().getString(R.string.heartrate, String.valueOf(heartRate)));
    }
}
